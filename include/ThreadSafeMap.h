#ifndef THREADSAFEMAP_H
#define THREADSAFEMAP_H
#include <mutex>
#include <unordered_map>

template <class K, class V>
class ThreadSafeMap : public std::enable_shared_from_this<ThreadSafeMap<K, V>> {
public:
	ThreadSafeMap() {};

	[[nodiscard]] std::unordered_map<K,V> GetCopy() {
		_mtx.lock();
		std::unordered_map<K,V> map = *_map;
		_mtx.unlock();
		return map;
	}

	void Insert(K const &key, V const &value) noexcept {
		_mtx.lock();

		_map[key] = value;
		_mtx.unlock();
	}

	auto Begin() {
		return _map.begin();
	}

	auto End() {
		return _map.end();
	}

	size_t Size() const {
		return _map.size();
	}

	V& At(K const &key) {
		return _map.at(key);
	}

private:
	std::unordered_map<K, V> _map;
	std::mutex _mtx;
};
#endif // !HTTPCLIENT_H
